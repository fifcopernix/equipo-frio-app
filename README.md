# Installation setup:

1. Get Cordova and Ionic installed:
  
    * `npm install -g cordova`

    * `npm install -g ionic`

2. Project is written with `Typescript`, so install it: `npm install -g typescript`

    * Additionally, if using Sublime Text 2 or 3, there's a package called `Typescript` for syntax highlights.

2. Install other project dependencies: `npm install`

3. Run ionic server: `ionic serve`

    * Run server in all platforms: `ionic serve -l`
